// ------DATA-----

db.users.insertMany ([
	 {
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "09191234567",
			email: "jdoe@email.com",		
		},
		courses: ["Python","React","PHP"],
		department: "HR"
	 },
	 {
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "09171234567",
			email: "shawking@email.com",		
		},
		courses: ["React","Laravel","SASS"],
		department: "HR"
	 },
	 {
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "09171234567",
			email: "neilarmstrong@email.com",		
		},
		courses: ["React","Laravel","SASS"],
		department: "HR"
	 }
])


// -------Activity No.2 - Find users with letter s in their first name or d in last name--------

db.users.find({
	$or: [
		{firstName: {$regex: "s",$options: "$i"}},
			
		{lastName: {$regex: "d",$options: "$i"}},
              ]
}, {firstName: 1, lastName: 1, _id: 0})



// -----Activity No. 3 - users form Hr dept. and their age is greater >= 70
db.users.find({
	age: { $gte: 70 },
	department: "HR"
})


// ------Activity No. 4 - users with letter e in their firstName and has an age of <= 30 

db.users.find({
	$and: [
		{
			age: {$lte:30}
		},
		{
			firstName: {$regex: 'e',$options: "i"}
		}
	]
})







